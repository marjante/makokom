using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using MakoKom.EntityFramework;

namespace MakoKom.Migrator
{
    [DependsOn(typeof(MakoKomDataModule))]
    public class MakoKomMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<MakoKomDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}