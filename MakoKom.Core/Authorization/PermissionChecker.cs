﻿using Abp.Authorization;
using MakoKom.Authorization.Roles;
using MakoKom.Authorization.Users;

namespace MakoKom.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
