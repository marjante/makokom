﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entites
{
    public class Taxes
    {
        public int Id { get; set; }
        public string NameOfTax { get; set; }
        public int Tax { get; set; }

    }
}
