﻿namespace MakoKom
{
    public class MakoKomConsts
    {
        public const string LocalizationSourceName = "MakoKom";

        public const bool MultiTenancyEnabled = true;
    }
}