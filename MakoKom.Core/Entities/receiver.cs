﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Reciver")]
    public class Receiver : BaseAuditedEntity
    {
        public int Number { get; set; }
        public int PartnerId { get; set; }
        public virtual Partner Partner { get; set; }
        public int ArticalId { get; set; }
        public virtual Artical Artical { get; set;}
        public string Factura { get; set;}

    }
}
