﻿using MakoKom.Authorization.Users;
using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Bills")]
    public class Bill : BaseAuditedEntity
    {
        public int Number { get; set; }
       
        public bool IsOpen { get; set; }
        public bool IsPay { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }


        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int PartnerId { get; set; }
        public virtual Partner Partner { get; set; }
        public List<Artical> Articals { get; set; }
        public int ArticalId { get; set; }
        public virtual Artical Artical { get; set; }


       
    }
}
