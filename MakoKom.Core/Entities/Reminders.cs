﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Reminder")]
    public class Reminders : BaseAuditedEntity
    {
        public string NameOfReminder { get; set; }
        public string Message { get; set; }
        public DateTime DueDate { get; set; }
    }
}
