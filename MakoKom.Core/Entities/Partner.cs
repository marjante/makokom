﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Partners")]
    public class Partner : BaseAuditedEntity
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Adress { get; set; }
        public int Phone { get; set; }
        public int AccountNubmer { get; set; }
        public string TaxNumber { get; set; }
        public string Email { get; set; }
        public int Percent { get; set; }


    }
}
