﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities.Base
{
    public abstract class BaseAuditedEntity : IEntity
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long LastModifierUserId { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public virtual long CreatorUserId { get; set; }

        public BaseAuditedEntity()
        {
            CreationTime = DateTime.Now;     
        }
        public bool IsTransient()
        {
            return true;
        }
    }
}
