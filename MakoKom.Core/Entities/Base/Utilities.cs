﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities.Base
{

        public interface ICreationTime
        {
            DateTime CreationTime { get; set; }
        }

        public interface ICreatedBy
        {
            long CreatorUserId { get; set; }
        }
    
}
