﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Articals")]
    public class Artical : BaseAuditedEntity
    {
        public string Name { get; set; }
        public double Stash { get; set; }
        public double PriceWithoutTax { get; set; }


        public string Description { get; set; }
        public int TaxeId { get; set; }
        public virtual Taxe Taxe { get; set; }
        public int MeasuresId { get; set; }
        public virtual Measures Measures { get; set; }

        public double CalculatePrice { get; set; }
        


    }
}
