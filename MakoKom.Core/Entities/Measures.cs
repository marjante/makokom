﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Measure")]
    public  class Measures : BaseAuditedEntity
    {
        public string MeasureName { get; set; }
    }
}
