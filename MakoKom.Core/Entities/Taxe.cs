﻿using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Entities
{
    [Table("Taxes")]
    public class Taxe : BaseAuditedEntity
    {
        public string NameOfTax { get; set; }
        public int Tax { get; set; }
    }
}
