﻿using Abp.MultiTenancy;
using MakoKom.Authorization.Users;

namespace MakoKom.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}