namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ListAddInBill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articals", "Bill_Id", c => c.Int());
            AddColumn("dbo.Bills", "SubTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Bills", "Total", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.Articals", "Bill_Id");
            AddForeignKey("dbo.Articals", "Bill_Id", "dbo.Bills", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articals", "Bill_Id", "dbo.Bills");
            DropIndex("dbo.Articals", new[] { "Bill_Id" });
            DropColumn("dbo.Bills", "Total");
            DropColumn("dbo.Bills", "SubTotal");
            DropColumn("dbo.Articals", "Bill_Id");
        }
    }
}
