namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateArtical2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Articals", "Taxe_Id", "dbo.Taxes");
            DropIndex("dbo.Articals", new[] { "Taxe_Id" });
            RenameColumn(table: "dbo.Articals", name: "Taxe_Id", newName: "TaxeId");
            AlterColumn("dbo.Articals", "TaxeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articals", "TaxeId");
            AddForeignKey("dbo.Articals", "TaxeId", "dbo.Taxes", "Id", cascadeDelete: true);
            DropColumn("dbo.Articals", "TaxId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articals", "TaxId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Articals", "TaxeId", "dbo.Taxes");
            DropIndex("dbo.Articals", new[] { "TaxeId" });
            AlterColumn("dbo.Articals", "TaxeId", c => c.Int());
            RenameColumn(table: "dbo.Articals", name: "TaxeId", newName: "Taxe_Id");
            CreateIndex("dbo.Articals", "Taxe_Id");
            AddForeignKey("dbo.Articals", "Taxe_Id", "dbo.Taxes", "Id");
        }
    }
}
