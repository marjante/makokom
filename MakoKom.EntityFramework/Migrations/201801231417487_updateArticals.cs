namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateArticals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articals", "TaxId", c => c.Int(nullable: false));
            AddColumn("dbo.Articals", "Taxe_Id", c => c.Int());
            CreateIndex("dbo.Articals", "Taxe_Id");
            AddForeignKey("dbo.Articals", "Taxe_Id", "dbo.Taxes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articals", "Taxe_Id", "dbo.Taxes");
            DropIndex("dbo.Articals", new[] { "Taxe_Id" });
            DropColumn("dbo.Articals", "Taxe_Id");
            DropColumn("dbo.Articals", "TaxId");
        }
    }
}
