namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateArtical4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articals", "CalculatePrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articals", "CalculatePrice");
        }
    }
}
