﻿using MakoKom.EntityFramework;
using EntityFramework.DynamicFilters;

namespace MakoKom.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly MakoKomDbContext _context;

        public InitialHostDbBuilder(MakoKomDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
