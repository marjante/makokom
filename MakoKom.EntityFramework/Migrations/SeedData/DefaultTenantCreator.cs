using System.Linq;
using MakoKom.EntityFramework;
using MakoKom.MultiTenancy;

namespace MakoKom.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly MakoKomDbContext _context;

        public DefaultTenantCreator(MakoKomDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
