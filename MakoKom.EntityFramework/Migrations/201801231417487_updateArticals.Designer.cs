// <auto-generated />
namespace MakoKom.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class updateArticals : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateArticals));
        
        string IMigrationMetadata.Id
        {
            get { return "201801231417487_updateArticals"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
