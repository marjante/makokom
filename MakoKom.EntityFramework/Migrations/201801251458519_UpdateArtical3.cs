namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateArtical3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articals", "MeasureId", c => c.Int(nullable: false));
            AddColumn("dbo.Articals", "Measures_Id", c => c.Int());
            CreateIndex("dbo.Articals", "Measures_Id");
            AddForeignKey("dbo.Articals", "Measures_Id", "dbo.Measure", "Id");
            DropColumn("dbo.Articals", "Measure");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articals", "Measure", c => c.String());
            DropForeignKey("dbo.Articals", "Measures_Id", "dbo.Measure");
            DropIndex("dbo.Articals", new[] { "Measures_Id" });
            DropColumn("dbo.Articals", "Measures_Id");
            DropColumn("dbo.Articals", "MeasureId");
        }
    }
}
