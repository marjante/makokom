namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddetReminder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reminder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameOfReminder = c.String(),
                        Message = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Reminder");
        }
    }
}
