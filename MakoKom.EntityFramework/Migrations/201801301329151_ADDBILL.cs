namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADDBILL : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        IsOpen = c.Boolean(nullable: false),
                        IsPay = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        PartnerId = c.Int(nullable: false),
                        ArticalId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(nullable: false),
                        User_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articals", t => t.ArticalId, cascadeDelete: true)
                .ForeignKey("dbo.Partners", t => t.PartnerId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.User_Id)
                .Index(t => t.PartnerId)
                .Index(t => t.ArticalId)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bills", "User_Id", "dbo.AbpUsers");
            DropForeignKey("dbo.Bills", "PartnerId", "dbo.Partners");
            DropForeignKey("dbo.Bills", "ArticalId", "dbo.Articals");
            DropIndex("dbo.Bills", new[] { "User_Id" });
            DropIndex("dbo.Bills", new[] { "ArticalId" });
            DropIndex("dbo.Bills", new[] { "PartnerId" });
            DropTable("dbo.Bills");
        }
    }
}
