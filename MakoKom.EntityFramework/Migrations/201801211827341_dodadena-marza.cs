namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodadenamarza : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Partners", "Percent", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Partners", "Percent");
        }
    }
}
