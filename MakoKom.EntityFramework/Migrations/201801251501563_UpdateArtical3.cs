namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateArtical3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articals", "MeasuresId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articals", "MeasuresId");
            AddForeignKey("dbo.Articals", "MeasuresId", "dbo.Measure", "Id", cascadeDelete: true);
            DropColumn("dbo.Articals", "Measure");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articals", "Measure", c => c.String());
            DropForeignKey("dbo.Articals", "MeasuresId", "dbo.Measure");
            DropIndex("dbo.Articals", new[] { "MeasuresId" });
            DropColumn("dbo.Articals", "MeasuresId");
        }
    }
}
