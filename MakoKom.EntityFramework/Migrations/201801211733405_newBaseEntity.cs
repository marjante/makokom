namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newBaseEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Partners", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Partners", "DeleterUserId", c => c.Long());
            AddColumn("dbo.Partners", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.Partners", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.Partners", "LastModifierUserId", c => c.Long(nullable: false));
            AddColumn("dbo.Partners", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Partners", "CreatorUserId", c => c.Long(nullable: false));
            AddColumn("dbo.Taxes", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Taxes", "DeleterUserId", c => c.Long());
            AddColumn("dbo.Taxes", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.Taxes", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.Taxes", "LastModifierUserId", c => c.Long(nullable: false));
            AddColumn("dbo.Taxes", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Taxes", "CreatorUserId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Taxes", "CreatorUserId");
            DropColumn("dbo.Taxes", "CreationTime");
            DropColumn("dbo.Taxes", "LastModifierUserId");
            DropColumn("dbo.Taxes", "LastModificationTime");
            DropColumn("dbo.Taxes", "DeletionTime");
            DropColumn("dbo.Taxes", "DeleterUserId");
            DropColumn("dbo.Taxes", "IsDeleted");
            DropColumn("dbo.Partners", "CreatorUserId");
            DropColumn("dbo.Partners", "CreationTime");
            DropColumn("dbo.Partners", "LastModifierUserId");
            DropColumn("dbo.Partners", "LastModificationTime");
            DropColumn("dbo.Partners", "DeletionTime");
            DropColumn("dbo.Partners", "DeleterUserId");
            DropColumn("dbo.Partners", "IsDeleted");
        }
    }
}
