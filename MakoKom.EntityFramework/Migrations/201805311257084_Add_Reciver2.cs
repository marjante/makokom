namespace MakoKom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Reciver2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reciver",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        PartnerId = c.Int(nullable: false),
                        ArticalId = c.Int(nullable: false),
                        Factura = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articals", t => t.ArticalId, cascadeDelete: true)
                .ForeignKey("dbo.Partners", t => t.PartnerId, cascadeDelete: true)
                .Index(t => t.PartnerId)
                .Index(t => t.ArticalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reciver", "PartnerId", "dbo.Partners");
            DropForeignKey("dbo.Reciver", "ArticalId", "dbo.Articals");
            DropIndex("dbo.Reciver", new[] { "ArticalId" });
            DropIndex("dbo.Reciver", new[] { "PartnerId" });
            DropTable("dbo.Reciver");
        }
    }
}
