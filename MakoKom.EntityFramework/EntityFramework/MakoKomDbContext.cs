﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using MakoKom.Authorization.Roles;
using MakoKom.Authorization.Users;
using MakoKom.MultiTenancy;
using System.Data.Entity;
using MakoKom.Entities;

namespace MakoKom.EntityFramework
{
    public class MakoKomDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public MakoKomDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in MakoKomDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of MakoKomDbContext since ABP automatically handles it.
         */
        public MakoKomDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public MakoKomDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public MakoKomDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
        #region ENTITIES

        public DbSet<Taxe> Taxes { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Artical> Articals { get; set; }
        public DbSet<Measures> Measure { get; set; }
        public DbSet<Bill>  Bill { get; set; }
        public DbSet<Reminders> Reminder { get; set; }
        public DbSet<Receiver> Reciver { get; set; }


        #endregion
    }
}
