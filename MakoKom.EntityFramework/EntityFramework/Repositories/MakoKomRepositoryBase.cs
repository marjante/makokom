﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace MakoKom.EntityFramework.Repositories
{
    public abstract class MakoKomRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<MakoKomDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected MakoKomRepositoryBase(IDbContextProvider<MakoKomDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class MakoKomRepositoryBase<TEntity> : MakoKomRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected MakoKomRepositoryBase(IDbContextProvider<MakoKomDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
