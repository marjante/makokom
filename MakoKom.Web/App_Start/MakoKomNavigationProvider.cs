﻿using Abp.Application.Navigation;
using Abp.Localization;
using MakoKom.Authorization;

namespace MakoKom.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class MakoKomNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Home",
                        new LocalizableString("HomePage", MakoKomConsts.LocalizationSourceName),
                        url: "#/",
                        icon: "fa fa-home",
                        requiresAuthentication: true
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Tenants",
                        L("Tenants"),
                        url: "#tenants",
                        icon: "fa fa-globe",
                        requiredPermissionName: PermissionNames.Pages_Tenants
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Users",
                        L("Users"),
                        url: "#users",
                        icon: "fa fa-users",
                        requiredPermissionName: PermissionNames.Pages_Users
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Roles",
                        L("Roles"),
                        url: "#users",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Roles
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "About",
                        new LocalizableString("About", MakoKomConsts.LocalizationSourceName),
                        url: "#/about",
                        icon: "fa fa-info"
                        )
                )
                  .AddItem(
                    new MenuItemDefinition(
                        "Taxes",
                        new LocalizableString("Taxes", MakoKomConsts.LocalizationSourceName),
                        url: "#/taxes",
                        icon: "fa fa-tag"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Partners",
                        new LocalizableString("Partners", MakoKomConsts.LocalizationSourceName),
                        url: "#/partners",
                        icon: "fa fa-users"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Articals",
                        new LocalizableString("Articals", MakoKomConsts.LocalizationSourceName),
                        url: "#/articals",
                        icon: "fa fa-users"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Measure",
                        new LocalizableString("Measure", MakoKomConsts.LocalizationSourceName),
                        url: "#/measure",
                        icon: "fa fa-users"
                        )
                 )
                 .AddItem(
                    new MenuItemDefinition(
                        "NewBill",
                        new LocalizableString("NewBill", MakoKomConsts.LocalizationSourceName),
                        url: "#/newBill",
                        icon: "fa fa-users"
                        )
                 ).AddItem(
                    new MenuItemDefinition(
                        "OutOfStock",
                        new LocalizableString("OutOfStock", MakoKomConsts.LocalizationSourceName),
                        url: "#/outOfStock",
                        icon: "fa fa-users"
                        )
                 )
                 .AddItem(
                    new MenuItemDefinition(
                        "Reminder",
                        new LocalizableString("Remidner", MakoKomConsts.LocalizationSourceName),
                        url: "#/addReminder",
                        icon: "fa fa-users"
                        )
                 );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, MakoKomConsts.LocalizationSourceName);
        }
    }
}
