﻿using Abp.Web.Mvc.Views;

namespace MakoKom.Web.Views
{
    public abstract class MakoKomWebViewPageBase : MakoKomWebViewPageBase<dynamic>
    {

    }

    public abstract class MakoKomWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected MakoKomWebViewPageBase()
        {
            LocalizationSourceName = MakoKomConsts.LocalizationSourceName;
        }
    }
}