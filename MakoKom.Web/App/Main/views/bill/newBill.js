﻿(function () {
    angular.module('app').controller('app.views.bill.newBill', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.bills', '$window','$interval',
        function ($scope, $timeout, $uibModal, billsService, $window, $interval) {
            var vm = this;
            
            vm.bills = [];
            $scope.partners = [];

            vm.openAddArtical = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/bill/addArticalToBill.cshtml',
                    controller: 'app.views.bill.addArticalToBill as vm',
                    backdrop: 'static'
                });    
                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });
           
            };

            vm.addPartner = function () {
                billsService.getPartners()
                    .then(function (response) {
                        $scope.partners = angular.copy(response.data);
                });
            }
            vm.addPartner();

            vm.getDate = function () {
                $scope.date = new Date();
            }
            vm.getDate();
            vm.print = function () {
                abp.notify.success('Сметката се Печати');
                $timeout(function () {
                    $window.close()
                }, 5000);

            }
            //vm.addBuyer = function () {
            //    var modalInstance = $uibModal.open({
            //        templateUrl: '/App/Main/views/bill/addPartnerToBill.cshtml',
            //        controller: 'app.views.bill.addPartnerToBill as vm',
            //        backdrop: 'static'
            //    });

            //    modalInstance.rendered.then(function () {
            //        $.AdminBSB.input.activate();
            //    });

            //    modalInstance.result.then(function () {
            //        vm.addPartner();
            //    });
            //};
         
        }
    ]);
})();