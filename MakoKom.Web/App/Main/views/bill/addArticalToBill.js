﻿(function () {
    angular.module('app').controller('app.views.bill.addArticalToBill', [
        '$scope', '$timeout','$uibModalInstance', 'abp.services.app.bills',
        function ($scope, $timeout, $uibModalInstance, billsService) {
            var vm = this;
            $scope.bills = [];
            vm.itemInBill = [];
            vm.save = function () {
                billsService.newBill($scope.bills)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });

            };
          
          
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.taxes = [];

            vm.getTaxes = function () {
                billsService.getTaxe()
                    .then(function (response) {
                        vm.taxes = response.data;
                    });
            };

            vm.getTaxes();

            vm.measure = [];

            vm.getMesure = function () {
                billsService.getMeasure()
                    .then(function (response) {
                        vm.measure = response.data;
                    });
            };
          
            vm.getMesure();

            $scope.articals = [];

            $scope.setClickedRow = function (index) {
                $scope.selectedRow = index;
            }
         
            function getArticals() {
                billsService.getArt()
                    .then(function (response) {
                        $scope.articals = angular.copy(response.data);
                    });
            }
            
            getArticals();
            vm.searchArticals = function (input) {
                billsService.searchArtical(input)
                    .then(function (result) {
                        $scope.articals = result.data;                        
                    })
            };

            $scope.removeTagOnBackspace = function (event) {
                if (event.keyCode === 8) {
                    getArticals();
                }
             
            };

            $scope.onChange = function (articals) {
                selected = articals
                console.log("item : " + selected)
            }
            //$scope.onChange = function (item) {
                
            //    console.log("item : " + this.item.name)
            //}

            $scope.selectedRow = null;  // initialize our variable to null
            $scope.setClickedRow = function (index) {  //function that sets the value of selectedRow to current index
                $scope.selectedRow = index;
                console.log("item : " + index)
            }
            
 
        }
    ]);
})();