﻿(function () {
    angular.module('app').controller('app.views.taxes.editTax', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxes','taxes',
        function ($scope, $uibModalInstance, taxesService,taxes) {
            var vm = this;

            vm.taxes = taxes;

            vm.save = function () {
                taxesService.update(taxes)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();