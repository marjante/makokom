﻿(function () {
    angular.module('app').controller('app.views.taxes.addTax', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxes',
        function ($scope, $uibModalInstance, taxesService) {
            var vm = this;

          
            vm.save = function () {
                taxesService.newTax(vm.taxes)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
               
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();