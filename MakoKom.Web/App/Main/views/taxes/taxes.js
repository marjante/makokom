﻿(function () {
    angular.module('app').controller('app.views.taxes.taxes', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.taxes', '$window',
        function ($scope, $timeout, $uibModal, taxesService,$window) {
            var vm = this;

            vm.taxes = [];

            vm.getTax = function () {
                taxesService.getTaxes().then(function (result) {
                    vm.taxes = result.data;
                });
            }

    

            vm.openAddTax = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/taxes/addTax.cshtml',
                    controller: 'app.views.taxes.addTax as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getTax();
                });
            };

            vm.editTax = function (taxes) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/taxes/editTax.cshtml',
                    controller: 'app.views.taxes.editTax as vm',
                    backdrop: 'static',
                    resolve: {
                        taxes: function () {
                            return taxes;
                        }
                    }
                });

                modalInstance.rendered.then(function () {               
                        $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getTaxes();
                });
            };

            vm.delete = function (taxes) {
                if ($window.confirm("Дали сакате да го избришете овој данок '" + taxes.nameOfTax + "'?")) {
                    taxesService.delete(taxes).then(function () {
                        abp.notify.info("Овој Данок е избришан '" + taxes.nameOfTax + "'.");
                        vm.getTax();
                    });
                }
            }

       
            //vm.delete = function (taxes) {
            //    abp.message.confirm("Delete user '" + taxes.nameOfTax + "'?",
            //        function (result) {
            //            console.dir(result);
            //            if (result) {
            //                taxesService.delete()
            //                    .success(function (deletingResult) {
            //                        if (deletingResult) {
            //                            abp.notify.info("Deleted user: " + taxes.nameOfTax);
            //                            vm.getTax();
            //                        }
            //                    });
            //            }
            //        });
            //}



            vm.refresh = function () {
                vm.getTax();
            };

            vm.getTax();
        }
    ]);
})();