﻿(function () {
    var controllerId = 'app.views.layout.topbar';
    angular.module('app').controller(controllerId, [
        '$rootScope', '$scope','$timeout', '$state', 'appSession',
        function ($rootScope, $scope, $timeout,$state, appSession) {
            var vm = this;
            vm.languages = [];
            vm.currentLanguage = {};

            function init() {
                vm.languages = abp.localization.languages;
                vm.currentLanguage = abp.localization.currentLanguage;
            }

            vm.changeLanguage = function (languageName) {
                location.href = abp.appPath + 'AbpLocalization/ChangeCulture?cultureName=' + languageName + '&returnUrl=' + window.location.pathname + window.location.hash;
            }

            init();
            function TimeCtrl($scope, $timeout) {
                $scope.clock = "Датум"; // initialise the time variable
                $scope.tickInterval = 1000 //ms

                var tick = function () {
                    $scope.clock = Date.now() // get the current time
                    $timeout(tick, $scope.tickInterval); // reset the timer
                }

                // Start the timer
                $timeout(tick, $scope.tickInterval);
            }
            TimeCtrl($scope, $timeout)

        }
    ]);
})();