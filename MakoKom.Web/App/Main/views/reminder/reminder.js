﻿(function () {
    angular.module('app').controller('app.views.reminder.reminder', [
        '$scope', '$uibModalInstance', 'abp.services.app.reminder',
        function ($scope, $uibModalInstance, reminderService) {
            var vm = this;

            vm.save = function () {
                reminderService.addReminder(vm.reminder)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            
        }
    ]);
})();