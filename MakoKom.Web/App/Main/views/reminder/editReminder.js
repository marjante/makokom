﻿(function () {
    angular.module('app').controller('app.views.reminder.editReminder', [
        '$scope', '$uibModalInstance', 'abp.services.app.reminder','reminder',
        function ($scope, $uibModalInstance, reminderService, reminder) {
            var vm = this;

            vm.reminder = reminder;

            vm.save = function () {
                reminderService.update(reminder)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();