﻿(function () {
    angular.module('app').controller('app.views.measure.editMeasure', [
        '$scope', '$uibModalInstance', 'abp.services.app.measure', 'measure',
        function ($scope, $uibModalInstance, measureService, meausre) {
            var vm = this;

            vm.measure = meausre;

            vm.save = function () {
                measureService.update(meausre)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();