﻿(function () {
    angular.module('app').controller('app.views.measure.addMeasure', [
        '$scope', '$uibModalInstance', 'abp.services.app.measure',
        function ($scope, $uibModalInstance, measureService) {
            var vm = this;

            vm.save = function () {
                measureService.create(vm.measure)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();