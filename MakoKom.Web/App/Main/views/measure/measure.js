﻿(function () {
    angular.module('app').controller('app.views.measure.measure', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.measure', '$window',
        function ($scope, $timeout, $uibModal, measureService, $window) {
            var vm = this;

            vm.measure = [];

            vm.getMeasure = function () {
                measureService.getAll().then(function (result) {
                    vm.measure = result.data;
                });
            }



            vm.openAdd = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/measure/addMeasure.cshtml',
                    controller: 'app.views.measure.addMeasure as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getMeasure();
                });
            };
            vm.edit = function (measure) {
                console.log(measure)
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/measure/editMeasure.cshtml',
                    controller: 'app.views.measure.editMeasure as vm',
                    backdrop: 'static',
                    resolve: {
                        measure: function () {
                            return measure;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getMeasure();
                });
            };
         

            vm.delete = function (measure) {
                if ($window.confirm("Дали сакате да ја избришете оваа мерка '" + measure.measureName + "'?")) {
                    measureService.delete(measure).then(function () {
                        abp.notify.info("Оваа мерка е избришана '" + measure.measureName + "'.");
                        vm.getMeasure();
                    });
                }
            }


   


            //vm.refresh = function () {
            //    vm.getMeasure();
            //};

            vm.getMeasure();
        }
    ]);
})();