﻿(function () {
    var controllerId = 'app.views.home';
    angular.module('app').controller(controllerId, [
        '$scope', '$timeout', 'dateFilter', '$uibModal', 'abp.services.app.articals', 'abp.services.app.partners', 'abp.services.app.reminder', '$window',
        function ($scope, $timeout, dateFilter, $uibModal, articalsService, partnersService, reminderService, $window) {
            var vm = this;

            vm.countOutOfStock = 0;
            $scope.countPartner = 0;
            vm.countArtical = 0;
            vm.reminders = [];
            vm.pastEvents = [];
            vm.totalRemendersCount = 0;
            vm.totalPastEvents = 0;
            vm.totalPagesCount = [];
            vm.pageConstant = 5;
            vm.page = 0;
            vm.totalPagesCount2 = [];
            vm.limitResults2 = 0;
            vm.limitResultsFrom2 = 0;
            vm.page2 = 0;

           
     
            // Reminder
            vm.openAddReminder = function () {
                var modalInstance = $uibModal.open({

                    templateUrl: '/App/Main/views/reminder/reminder.cshtml',
                    controller: 'app.views.reminder.reminder as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getAllReminders();
                });
            };

            vm.details = function (reminder) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/reminder/remDetails.cshtml',
                    controller: 'app.views.reminder.editReminder as vm',
                    backdrop: 'static',
                    resolve: {
                        reminder: function () {
                            reminder.dueDate = new Date(reminder.dueDate)
                            return reminder;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });


            };
            vm.editReminder = function (reminder) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/reminder/editReminder.cshtml',
                    controller: 'app.views.reminder.editReminder as vm',
                    backdrop: 'static',
                    resolve: {
                        reminder: function () {
                           
                            reminder.dueDate = new Date(reminder.dueDate);
                            return reminder;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });
                modalInstance.result.then(function () {
                    vm.getAllReminders();
                    vm.getPastReminders();
                });
            }
            
            vm.delete = function (reminder) {
                if ($window.confirm("Дали сакате да го избришете овој Потсетник '" + reminder.nameOfReminder + "'?")) {
                    reminderService.delete(reminder).then(function () {
                        abp.notify.info("Овој Потсетник е избришан '" + reminder.nameOfReminder + "'.");
                        vm.getAllReminders();
                        vm.getPastReminders();
                        
                    });
                }
            }
           
            vm.refresh = function () {
                vm.getAllReminders();
                vm.getPastReminders();
                
            };
            //function getAllReminders() {
            //    reminderService.getAll().then(function (result) {
            //        vm.reminders = result.data;
            //    })
            //}
            //    getAllReminders();

            vm.getAllReminders = function (page) {
                vm.pageCheckUndefined = page !== undefined ? page : page === "" ? page : 1;
                vm.skipCount = (vm.pageCheckUndefined - 1) * vm.pageConstant;
               

                abp.ui.setBusy(null, reminderService.remidersAll({
                    skipCount: vm.skipCount,
                }).then(function (response) {
                    vm.reminders = response.data.items;
                    vm.totalRemendersCount = response.data.totalCount;
                    vm.countPages = Math.ceil(parseFloat(vm.totalRemendersCount) / vm.pageConstant);
                    vm.totalPagesCount = [];
                    vm.limitResults = 0;
                    vm.limitResultsFrom = 0;
                    vm.otherButtons = [];
                  

                    for (var i = 0; i < vm.countPages; i++) {
                        vm.totalPagesCount.push(i + 1);
                    }

                    switch (vm.countPages) {
                        case 3:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults = 3;
                                    vm.limitResultsFrom = -1;
                                    break;
                                case 2:
                                    vm.limitResults = 3;
                                    vm.limitResultsFrom = -2;
                                    break;
                                default:
                                    vm.limitResults = 3;
                                    vm.limitResultsFrom = -3;
                            }
                            break;
                        case 4:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults = 4;
                                    vm.limitResultsFrom = -1;
                                    break;
                                case 2:
                                    vm.limitResults = 4;
                                    vm.limitResultsFrom = -2;
                                    break;
                                case 3:
                                    vm.limitResults = 4;
                                    vm.limitResultsFrom = -3;
                                    break;
                                default:
                                    vm.limitResults = 4;
                                    vm.limitResultsFrom = -4;
                            }
                            break;
                        default:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults = 5;
                                    vm.limitResultsFrom = -1;
                                    break;
                                case 2:
                                    vm.limitResults = 5;
                                    vm.limitResultsFrom = -2;
                                    break;
                                case vm.countPages - 1:
                                    vm.limitResults = 6;
                                    vm.limitResultsFrom = -4;
                                    break;
                                case vm.countPages:
                                    vm.limitResults = 7;
                                    vm.limitResultsFrom = -5;
                                    break;
                                default:
                                    vm.limitResults = 5;
                                    vm.limitResultsFrom = -3;
                            }
                    }

                    $scope.addClass();
                })
                );
            }
            $scope.addClass = function () {
                $timeout(function () {
                    document.getElementById(vm.pageCheckUndefined).classList.add('btn-primary');
                    document.getElementById(vm.pageCheckUndefined).style.cursor = 'default';
                    document.getElementById(vm.pageCheckUndefined).style.pointerEvents = 'none';
                }, 100).then(function () {
                    switch (vm.countPages) {
                        case 3:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons = [2, 3];
                                    $scope.forLoopClassCheck();
                                    break;
                                case 2:
                                    vm.otherButtons = [1, 3];
                                    $scope.forLoopClassCheck();
                                    break;
                                default:
                                    vm.otherButtons = [1, 2];
                                    $scope.forLoopClassCheck();
                            }
                            break;
                        case 4:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons = [2, 3, 4];
                                    $scope.forLoopClassCheck();
                                    break;
                                case 2:
                                    vm.otherButtons = [1, 3, 4];
                                    $scope.forLoopClassCheck();
                                    break;
                                case 3:
                                    vm.otherButtons = [1, 2, 4];
                                    $scope.forLoopClassCheck();
                                    break;
                                default:
                                    vm.otherButtons = [1, 2, 3];
                                    $scope.forLoopClassCheck();
                            }
                            break;
                        default:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons = [2, 3, 4, 5];
                                    $scope.forLoopClassCheck();
                                    break;
                                case 2:
                                    vm.otherButtons = [1, 3, 4, 5];
                                    $scope.forLoopClassCheck();
                                    break;
                                case vm.countPages - 1:
                                    vm.otherButtons = [vm.pageCheckUndefined - 3, vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1, vm.pageCheckUndefined + 1];
                                    $scope.forLoopClassCheck();
                                    break;
                                case vm.countPages:
                                    vm.otherButtons = [vm.pageCheckUndefined - 4, vm.pageCheckUndefined - 3, vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1];
                                    $scope.forLoopClassCheck();
                                    break;
                                default:
                                    vm.otherButtons = [vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1, vm.pageCheckUndefined + 1, vm.pageCheckUndefined + 2];
                                    $scope.forLoopClassCheck();
                            }
                    }
                });
            };
            $scope.forLoopClassCheck = function () {
                for (var j = 0; j < vm.otherButtons.length; j++) {
                    if (document.getElementById(vm.otherButtons[j]).classList.contains('btn-primary')) {
                        document.getElementById(vm.otherButtons[j]).classList.remove('btn-primary');
                    }
                    if (document.getElementById(vm.otherButtons[j]).hasAttribute('style')) {
                        document.getElementById(vm.otherButtons[j]).removeAttribute('style');
                    }
                }
            }

            vm.getPastReminders = function (page2) {
                vm.pageCheckUndefined = page2 !== undefined ? page2 : page2 === "" ? page2 : 1;
                vm.skipCount = (vm.pageCheckUndefined - 1) * vm.pageConstant;


                abp.ui.setBusy(null, reminderService.getPastRemenders({
                    skipCount: vm.skipCount,
                }).then(function (response) {
                    vm.pastEvents = response.data.items;
                    vm.totalPastEvents = response.data.totalCount;
                    vm.countPages2 = Math.ceil(parseFloat(vm.totalPastEvents) / vm.pageConstant);
                    vm.totalPagesCount2 = [];
                    vm.limitResults2 = 0;
                    vm.limitResultsFrom2 = 0;
                    vm.otherButtons2 = [];

                    for (var i = 0; i < vm.countPages2; i++) {
                        vm.totalPagesCount2.push(i + 1);
                    }

                    switch (vm.countPages2) {
                        case 3:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults2 = 3;
                                    vm.limitResultsFrom2 = -1;
                                    break;
                                case 2:
                                    vm.limitResults2 = 3;
                                    vm.limitResultsFrom2 = -2;
                                    break;
                                default:
                                    vm.limitResults2 = 3;
                                    vm.limitResultsFrom2 = -3;
                            }
                            break;
                        case 4:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults2 = 4;
                                    vm.limitResultsFrom2 = -1;
                                    break;
                                case 2:
                                    vm.limitResults2 = 4;
                                    vm.limitResultsFrom2 = -2;
                                    break;
                                case 3:
                                    vm.limitResults2 = 4;
                                    vm.limitResultsFrom2 = -3;
                                    break;
                                default:
                                    vm.limitResults2 = 4;
                                    vm.limitResultsFrom2 = -4;
                            }
                            break;
                        default:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.limitResults2 = 5;
                                    vm.limitResultsFrom2 = -1;
                                    break;
                                case 2:
                                    vm.limitResults2 = 5;
                                    vm.limitResultsFrom2 = -2;
                                    break;
                                case vm.countPages2 - 1:
                                    vm.limitResults2 = 6;
                                    vm.limitResultsFrom2 = -4;
                                    break;
                                case vm.countPages2:
                                    vm.limitResults2 = 7;
                                    vm.limitResultsFrom2 = -5;
                                    break;
                                default:
                                    vm.limitResults2 = 5;
                                    vm.limitResultsFrom2 = -3;
                            }
                    }

                    $scope.addClass2();
                })
                );
            }

            $scope.addClass2 = function () {
                $timeout(function () {
                    document.getElementById(vm.pageCheckUndefined).classList.add('btn-primary');
                    document.getElementById(vm.pageCheckUndefined).style.cursor = 'default';
                    document.getElementById(vm.pageCheckUndefined).style.pointerEvents = 'none';
                }, 100).then(function () {
                    switch (vm.countPages2) {
                        case 3:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons2 = [2, 3];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case 2:
                                    vm.otherButtons2 = [1, 3];
                                    $scope.forLoopClassCheck2();
                                    break;
                                default:
                                    vm.otherButtons2 = [1, 2];
                                    $scope.forLoopClassCheck2();
                            }
                            break;
                        case 4:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons2 = [2, 3, 4];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case 2:
                                    vm.otherButtons2 = [1, 3, 4];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case 3:
                                    vm.otherButtons2 = [1, 2, 4];
                                    $scope.forLoopClassCheck2();
                                    break;
                                default:
                                    vm.otherButtons2 = [1, 2, 3];
                                    $scope.forLoopClassCheck2();
                            }
                            break;
                        default:
                            switch (vm.pageCheckUndefined) {
                                case 1:
                                    vm.otherButtons2 = [2, 3, 4, 5];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case 2:
                                    vm.otherButtons2 = [1, 3, 4, 5];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case vm.countPages2 - 1:
                                    vm.otherButtons = [vm.pageCheckUndefined - 3, vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1, vm.pageCheckUndefined + 1];
                                    $scope.forLoopClassCheck2();
                                    break;
                                case vm.countPages2:
                                    vm.otherButtons2 = [vm.pageCheckUndefined - 4, vm.pageCheckUndefined - 3, vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1];
                                    $scope.forLoopClassCheck2();
                                    break;
                                default:
                                    vm.otherButtons2 = [vm.pageCheckUndefined - 2, vm.pageCheckUndefined - 1, vm.pageCheckUndefined + 1, vm.pageCheckUndefined + 2];
                                    $scope.forLoopClassCheck2();
                            }
                    }
                });
            };
            $scope.forLoopClassCheck2 = function () {
                for (var j = 0; j < vm.otherButtons2.length; j++) {
                    if (document.getElementById(vm.otherButtons2[j]).classList.contains('btn-primary')) {
                        document.getElementById(vm.otherButtons2[j]).classList.remove('btn-primary');
                    }
                    if (document.getElementById(vm.otherButtons2[j]).hasAttribute('style')) {
                        document.getElementById(vm.otherButtons2[j]).removeAttribute('style');
                    }
                }
            }
            vm.getAllReminders();
            vm.getPastReminders();

          //  vm.getPastReminders();
                //function getPastReminders() {
                //    reminderService.getPastEvents().then(function (result) {
                //        vm.pastEvents = result.data;
                //    })
                //}
                //getPastReminders();

           
             
            // END of Reminder
            var countOutOfStock = function () {
                articalsService.countOutOfStock().then(function (result) {
                    vm.countOutOfStock = result.data;  
                });
            }
            countPartner();

            function countPartner() {
                partnersService.countPartners().then(function (result) {
                    $scope.countPartner = result.data;                
                });
            }
          
            countArticals();
            function countArticals() {
                articalsService.countArticals().then(function (result) {
                    vm.countArtical = result.data;
                });
            }

            countOutOfStock();
            var init = function () {
              
                function initSummaries() {
                    //Widgets count
                    $('.count-to').countTo();
        
                    //Sales count to
                    $('.sales-count-to').countTo({
                        formatter: function (value, options) {
                            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
                        }
                    });
                }
               

                initSummaries();
    
            };

            init();

            

           

         
           
        }
    ]);
})();