﻿(function () {
    angular.module('app').controller('app.views.partners.editPartner', [
        '$scope', '$uibModalInstance', 'abp.services.app.partners','partner',
        function ($scope, $uibModalInstance, partnersService,partner) {
            var vm = this;

            vm.partners = partner;

            vm.save = function () {
                partnersService.update(partner)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();