﻿(function () {
    angular.module('app').controller('app.views.partners.addPartner', [
        '$scope', '$uibModalInstance', 'abp.services.app.partners',
        function ($scope, $uibModalInstance, partnersService) {
            var vm = this;

            vm.save = function () {
                partnersService.create(vm.partners)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

        }
    ]);
})();