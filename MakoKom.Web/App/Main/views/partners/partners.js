﻿(function () {
    angular.module('app').controller('app.views.partners.partners', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.partners','$window',
        function ($scope, $timeout, $uibModal, partnersService,$window) {
            var vm = this;

            vm.partners = [];
            vm.count = 0;
            function countPartner() {
                partnersService.countPartners().then(function (result) {
                    vm.count = result.data;
                });
            }
            countPartner();
            function getPartner() {
                partnersService.getPartners().then(function (result) {
                    vm.partners = result.data;
                });
            }

            vm.openAddPartner = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/partners/addPartner.cshtml',
                    controller: 'app.views.partners.addPartner as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    getPartner();
                    countPartner();
                });
            };

            vm.editPartner = function (partner) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/partners/editPartner.cshtml',
                    controller: 'app.views.partners.editPartner as vm',
                    backdrop: 'static',
                    resolve: {
                        partner: function () {
                            return partner;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    getPartner();
                });
            };

            vm.details = function (partner) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/partners/details.cshtml',
                    controller: 'app.views.partners.editPartner as vm',
                    backdrop: 'static',
                    resolve: {
                        partner: function () {
                            return partner;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

               
            };
            vm.delete = function (partner) {
                if ($window.confirm("Дали сакате да го избришете овој Партнер '" + partner.name + "'?")) {
                    partnersService.delete(partner).then(function () {
                        abp.notify.info("Овој Партнер е избришан '" + partner.name + "'.");
                        getPartner();
                    });
                }
            }
            vm.refresh = function () {
                getPartner();
            };
       
            getPartner();

        
            
            vm.searchPartners = function (input) {
                partnersService.searchPartner(input)
                    .then(function (result) {
                        vm.partners = result.data;
                         })
            };

            $scope.removeTagOnBackspace = function (event) {
                if (event.keyCode === 8) {
                    getPartner();
                }
            };


         


        }
    ]);
})();