﻿(function () {
    angular.module('app').controller('app.views.articals.addArtical', [
        '$scope', '$uibModalInstance', 'abp.services.app.articals',
        function ($scope, $uibModalInstance, articalsService) {
            var vm = this;


            vm.save = function () {
                articalsService.addArtical(vm.articals)
                    .then(function () {
                        abp.notify.info(App.localize('Артиклот е успешно додаден'));
                        $uibModalInstance.close();
                    });

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.taxes = [];

            vm.getTaxes = function () {
                articalsService.getTaxe()
                    .then(function (response) {
                        vm.taxes = response.data;
                    });
            };

            vm.getTaxes();

            vm.measure = [];

            vm.getMesure = function () {
                articalsService.getMesures()
                    .then(function (response) {
                        vm.measure = response.data;
                    });
            };

            vm.getMesure();
        }
    ]);
})();