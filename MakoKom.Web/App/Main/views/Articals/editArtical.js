﻿(function () {
    angular.module('app').controller('app.views.articals.editArtical', [
        '$scope', '$uibModalInstance', 'abp.services.app.articals', 'artical',
        function ($scope, $uibModalInstance, articalsService, artical) {
            var vm = this;

            vm.articals = artical;

            $scope.taxes = [];
            $scope.measures = [];


            vm.save = function () {
                articalsService.update(artical)
                    .then(function () {
                        abp.notify.info(App.localize('Снимањето е успешно'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
           

            vm.getTaxes = function () {
                articalsService.getTaxe()
                    .then(function (response) {
                        $scope.taxes = angular.copy(response.data);
                    });
            };

            vm.getTaxes();

     

            vm.getMesure = function () {
                articalsService.getMesures()
                    .then(function (response) {
                        $scope.measures = angular.copy(response.data);
                    });
            };

            vm.getMesure();
        }
    ]);
})();