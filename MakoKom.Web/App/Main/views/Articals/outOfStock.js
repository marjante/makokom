﻿(function () {
    angular.module('app').controller('app.views.articals.outOfStock', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.articals', '$window',
        function ($scope, $timeout, $uibModal, articalsService, $window) {
            var vm = this;

            vm.articals = 0;


            function getOutOfStock() {
                articalsService.outOfStock().then(function (result) {
                    vm.articals = result.data;
                });
            }

          
            getOutOfStock();

            
        }
    ]);
})();