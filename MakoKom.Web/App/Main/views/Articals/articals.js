﻿(function () {
    angular.module('app').controller('app.views.articals.articals', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.articals','$window',
        function ($scope, $timeout, $uibModal, articalsService, $window) {
            var vm = this;

            vm.articals = [];
            vm.count = 0;
            

            vm.getArticals = function () {
                articalsService.getAll().then(function (result) {
                    vm.articals = result.data;
                });
               
            }
            vm.getArtByName = function () {
                articalsService.getAll().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.getAll2().then(function (result) {
                    vm.articals = result.data;
                });
            }
            function count() {
                articalsService.countArticals().then(function (result) {
                    vm.count = result.data;
                });
            }
            
            count();
            vm.getArticlasByDescrpiton = function() {
                articalsService.getByDescription().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.getByDescription2().then(function (result) {
                    vm.articals = result.data;
                })
            }

            vm.getArtByStash = function () {
                articalsService.getByStash().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.getByStash2().then(function (result) {
                    vm.articals = result.data;
                })
            
            }
            vm.sortByPrice = function () {
                articalsService.sortByPrice().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.sortByPrice2().then(function (result) {
                    vm.articals = result.data;
                })

            }
            vm.sortByPriceWithTax = function () {
                articalsService.sortByPriceWithTax().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.sortByPriceWithTax2().then(function (result) {
                    vm.articals = result.data;
                })

            }
            vm.sortByDate = function () {
                articalsService.sortByDate().then(function (result) {
                    vm.articals = result.data;
                })
                articalsService.sortByDate2().then(function (result) {
                    vm.articals = result.data;
                })

            }
            vm.openAddArtical = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/articals/addArtical.cshtml',
                    controller: 'app.views.articals.addArtical as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                   vm.getArticals();
                   count();
                });
            };

            vm.edit = function (artical) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/articals/editArtical.cshtml',
                    controller: 'app.views.articals.editArtical as vm',
                    backdrop: 'static',
                    resolve: {
                        artical: function () {
                            return artical;
                        }
                    }                 
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    vm.getArticals();
                });
            };

            vm.refresh = function () {
                vm.getArticals();
            };

            vm.delete = function (artical) {
                if ($window.confirm("Дали сакате да го избришете овој Артикал '" + artical.name + "'?")) {
                    articalsService.delete(artical).then(function () {
                        abp.notify.info("Овој Артикал е избришан '" + artical.name + "'.");
                        vm.getArticals();
                        count();
                    });
                }
            }
            vm.refresh = function () {
                vm.getArticals();
            };

            vm.getArticals();

            vm.searchArticals = function (input) {
                articalsService.searchArtical(input)
                    .then(function (result) {
                        vm.articals = result.data;
                    })
            };

            $scope.removeTagOnBackspace = function (event) {
                if (event.keyCode === 8) {
                    vm.getArticals();
                }
            };
        }
    ]);
})();