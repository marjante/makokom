﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {
            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);

            if (abp.auth.hasPermission('Pages.Users')) {
                $stateProvider
                    .state('users', {
                        url: '/users',
                        templateUrl: '/App/Main/views/users/index.cshtml',
                        menu: 'Users' //Matches to name of 'Users' menu in MakoKomNavigationProvider
                    });
                $urlRouterProvider.otherwise('/users');
            }
            if (abp.auth.hasPermission('Pages.Users')) {
                $stateProvider
                    .state('measure', {
                        url: '/measure',
                        templateUrl: '/App/Main/views/measure/measure.cshtml',
                        menu: 'Measure' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                    });
                $urlRouterProvider.otherwise('/measure');
            }

            if (abp.auth.hasPermission('Pages.Roles')) {
                $stateProvider
                    .state('roles', {
                        url: '/roles',
                        templateUrl: '/App/Main/views/roles/index.cshtml',
                        menu: 'Roles' //Matches to name of 'Tenants' menu in MakoKomNavigationProvider
                    });
                $urlRouterProvider.otherwise('/roles');
            }

            if (abp.auth.hasPermission('Pages.Tenants')) {
                $stateProvider
                    .state('tenants', {
                        url: '/tenants',
                        templateUrl: '/App/Main/views/tenants/index.cshtml',
                        menu: 'Tenants' //Matches to name of 'Tenants' menu in MakoKomNavigationProvider
                    });
                $urlRouterProvider.otherwise('/tenants');
            }

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in MakoKomNavigationProvider
                })
                .state('about', {
                    url: '/about',
                    templateUrl: '/App/Main/views/about/about.cshtml',
                    menu: 'About' //Matches to name of 'About' menu in MakoKomNavigationProvider
                })
                .state('taxes', {
                    url: '/taxes',
                    templateUrl: '/App/Main/views/taxes/taxes.cshtml',
                    menu: 'Taxes' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                })
                .state('partners', {
                    url: '/partners',
                    templateUrl: '/App/Main/views/partners/partners.cshtml',
                    menu: 'Partners' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                })
                .state('articals', {
                    url: '/articals',
                    templateUrl: '/App/Main/views/articals/articals.cshtml',
                    menu: 'Articals' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                })
                .state('newBill', {
                    url: '/newBill',
                    templateUrl: '/App/Main/views/bill/newBill.cshtml',
                    menu: 'NewBill' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                })
             .state('outOfStock', {
                url: '/outOfStock',
                templateUrl: '/App/Main/views/articals/outOfStock.cshtml',
                menu: 'OutOfStock' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                })
                .state('addReminder', {
                    url: '/addReminder',
                    templateUrl: '/App/Main/views/reminder/reminder.cshtml',
                    menu: 'Reminder' //Matches to name of 'Taxes' menu in MakoKomNavigationProvider
                });
            
        }
    ]);

})();