using Abp.AutoMapper;
using MakoKom.Sessions.Dto;

namespace MakoKom.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}