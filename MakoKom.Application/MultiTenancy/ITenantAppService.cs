﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MakoKom.MultiTenancy.Dto;

namespace MakoKom.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
