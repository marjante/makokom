﻿using Abp.AutoMapper;
using MakoKom.Entites;
using MakoKom.Entites.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Services.Tax.Dto
{
    [AutoMapFrom(typeof(Taxes))]
    public class TaxesDto : BaseAuditedEntity
    {
        public string NameOfTax { get; set; }
        public int Tax { get; set; }
    }
}
