﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MakoKom.Sessions.Dto;

namespace MakoKom.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
