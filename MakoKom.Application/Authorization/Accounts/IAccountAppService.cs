﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MakoKom.Authorization.Accounts.Dto;

namespace MakoKom.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
