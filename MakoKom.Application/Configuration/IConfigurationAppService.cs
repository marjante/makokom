﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MakoKom.Configuration.Dto;

namespace MakoKom.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}