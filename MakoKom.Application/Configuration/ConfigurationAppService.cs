﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using MakoKom.Configuration.Dto;

namespace MakoKom.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : MakoKomAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
