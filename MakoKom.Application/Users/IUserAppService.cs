using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MakoKom.Roles.Dto;
using MakoKom.Users.Dto;

namespace MakoKom.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}