﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MakoKom.Authorization.Users;
using MakoKom.Entities;
using MakoKom.MyEntities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Bills.Dto
{
    [AutoMapFrom(typeof(Bill))]
    public class BillDto : BaseAuditedEntityDto, IEntityDto
    {
        public int Number { get; set; }

        public bool IsOpen { get; set; }
        public bool IsPay { get; set; }


        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int PartnerId { get; set; }
        public virtual Partner Partner { get; set; }
        public int ArticalId { get; set; }
        public virtual Artical Artical { get; set; }
    }
}
