﻿using Abp.Domain.Repositories;
using MakoKom.Authorization.Users;
using MakoKom.Entities;
using MakoKom.Users.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Bills
{
    public class BillsAppService : MakoKomAppServiceBase,  IBillsAppService
    {
        private readonly IRepository<Bill> _billRepo;
        private readonly IRepository<Partner> _partnerRepo;
        private readonly IRepository<User, long> _userRepo;
        private readonly IRepository<Artical> _articalRepo;
        private readonly IRepository<Measures> _measureRepo;
        private readonly IRepository<Taxe> _taxeRepo;


        public BillsAppService(IRepository<Bill> billRepo, IRepository<Partner> partnerRepo,IRepository<Artical> articalRepo,IRepository<User, long> userRepository, IRepository<Measures> measureRepo, IRepository<Taxe> taxeRepo)
        {
            _billRepo = billRepo;
            _partnerRepo = partnerRepo;       
            _articalRepo = articalRepo;
            _userRepo = userRepository;
            _measureRepo = measureRepo;
            _taxeRepo = taxeRepo;
        }

        public IEnumerable<Bill> GetAll()
        {
            var result = _billRepo.GetAllIncluding(x => x.Artical, x => x.Partner, x => x.User).Where(i => i.IsDeleted == false).ToList();
            return result;
        }
        public void NewBill(Bill input)
        {
            var bill = new Bill
            {
                Number = 5,
                IsDeleted = false,
                CreationTime = DateTime.Now,
                CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1,
                ArticalId = input.ArticalId,
                Artical = _articalRepo.FirstOrDefault(i => i.Id == input.Id),
                IsOpen = true,
                IsPay = true,
                SubTotal = 1000,
                Total = 500,
                PartnerId = 14,
                Partner = _partnerRepo.FirstOrDefault(i => i.Id == input.Id)
                
 
            };
            _billRepo.Insert(bill);
        }
      
    
        public IEnumerable<Taxe> GetTaxe()
        {
            return _taxeRepo.GetAll().Where(i => i.IsDeleted == false).ToList();

        }

        public IEnumerable<Measures> GetMeasure()
        {
            return _measureRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }
        public IEnumerable<Partner> GetPartners()
        {
            return _partnerRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }
        public IEnumerable<User> GetUser()
        {
            return _userRepo.GetAll().ToList();
        }
        public IEnumerable<Artical> GetArt()
        {
            return _articalRepo.GetAllIncluding(i=> i.Measures, i=> i.Taxe).Where(i => i.IsDeleted == false).OrderBy(i => i.Name).ToList();
        }
        public IEnumerable<Artical> SearchArtical(string input)
        {
            var result = _articalRepo.GetAllIncluding(i => i.Taxe, i => i.Measures)
                .Where(i => i.Name.ToUpper().Contains(input.Trim().ToUpper()))
                             .Where(i => i.IsDeleted == false)
                             .ToList();
            return result;

        }
    }
}
