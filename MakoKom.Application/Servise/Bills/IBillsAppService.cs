﻿using Abp.Application.Services;
using MakoKom.Authorization.Users;
using MakoKom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Bills
{
   public interface IBillsAppService : IApplicationService
    {
        IEnumerable<Bill> GetAll();
        void NewBill(Bill input);
        IEnumerable<Artical> GetArt();
        IEnumerable<Measures> GetMeasure();
        IEnumerable<Taxe> GetTaxe();
        IEnumerable<User> GetUser();
        IEnumerable<Partner> GetPartners();
        IEnumerable<Artical> SearchArtical(string input);
        

    }
}
