﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using MakoKom.Entities;
using MakoKom.Servise.Reminder.Dto;

namespace MakoKom.Servise.Reminder
{
    public class ReminderAppService : MakoKomAppServiceBase , IReminderAppService
    {
        private readonly IRepository<Reminders> _reminderRepo;

        public ReminderAppService(IRepository<Reminders> reminderRepo)
        {
            _reminderRepo = reminderRepo;
        }
        //public IEnumerable<Reminders> GetAll()
        //{
        //    var result = _reminderRepo.GetAllList().Where(i => i.IsDeleted == false).OrderByDescending(i => i.CreationTime);
        //    return result;
        //}
      public PagedResultDto<Reminders> RemidersAll(GetAllEventsInput input)
        {

            if (input.MaxResultCount <= 0)
            {
                input.MaxResultCount = 5;
            }
            var reminders = _reminderRepo.GetAll().Where(s => s.IsDeleted == false);

            var itemReminders = reminders.OrderByDescending(i => i.CreationTime).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            return new PagedResultDto<Reminders>
            {
                TotalCount = reminders.Count(),
                Items = itemReminders

            };
        }
        public int ReminderCount()
        {
            return _reminderRepo.GetAll().Where(i => i.IsDeleted == false).ToList().Count();
        }
        public PagedResultDto<Reminders> GetPastRemenders(GetAllEventsInput input)
        {

            if (input.MaxResultCount <= 0)
            {
                input.MaxResultCount = 5;
            }
            var reminders = _reminderRepo.GetAll().Where(s => s.IsDeleted == true);

            var itemReminders = reminders.OrderByDescending(i => i.CreationTime).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            return new PagedResultDto<Reminders>
            {
                TotalCount = reminders.Count(),
                Items = itemReminders

            };
        }
        //public IEnumerable<Reminders> GetPastEvents()
        //{
        //    var result = _reminderRepo.GetAllList().Where(i => i.IsDeleted == true).OrderByDescending(i => i.DeletionTime);
        //    return result;
        //}
        public void AddReminder(Reminders input)
        {
            var reminder = new Reminders
            {
                NameOfReminder = input.NameOfReminder,
                DueDate = input.DueDate,
                Message = input.Message,
                CreationTime = DateTime.Now,
                CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1,
            

        };
          
            _reminderRepo.Insert(reminder);
        }
        public void Update(Reminders input)
        {
            var rem = _reminderRepo.Get(input.Id);
            rem.DueDate = input.DueDate;
            rem.NameOfReminder = input.NameOfReminder;
            rem.LastModificationTime = DateTime.Now;
            rem.LastModifierUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;

        }
        public void Delete(Reminders input)
        {
           var del = _reminderRepo.Get(input.Id);
            del.IsDeleted = true;
            del.DeleterUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1; ;
            del.DeletionTime = DateTime.Now;

        }

    }
}
