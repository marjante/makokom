﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MakoKom.Entities;
using MakoKom.Servise.Reminder.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Reminder
{
   public interface IReminderAppService : IApplicationService
    {
        //IEnumerable<Reminders> GetAll();
        void AddReminder(Reminders input);
        void Update(Reminders input);
        void Delete(Reminders input);
       // IEnumerable<Reminders> GetPastEvents();
        int ReminderCount();
        PagedResultDto<Reminders> RemidersAll(GetAllEventsInput input);
        PagedResultDto<Reminders> GetPastRemenders(GetAllEventsInput input);

    }
}
