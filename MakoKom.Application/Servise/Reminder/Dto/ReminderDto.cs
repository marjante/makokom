﻿using Abp.Application.Services.Dto;
using MakoKom.MyEntities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Reminder.Dto
{
    public class ReminderDto : BaseAuditedEntityDto, IEntityDto
    {
        public string NameOfReminder { get; set; }
        public string Message { get; set; }
        public DateTime DueDate { get; set; }
    }
}
