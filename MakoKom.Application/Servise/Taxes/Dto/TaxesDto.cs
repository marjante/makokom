﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MakoKom.MyEntities.BaseEntity;
using MakoKom.Entities;


namespace MakoKom.Servise.Taxes.Dto
{
    [AutoMapFrom(typeof(Taxe))]
    public class TaxesDto : BaseAuditedEntityDto , IEntityDto
    {
        public string NameOfTax { get; set; }
        public int Tax { get; set; }
    }
}
