﻿using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using MakoKom.Servise.Taxes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MakoKom.Entities;
namespace MakoKom.Servise.Taxes
{
   public class TaxesAppService : MakoKomAppServiceBase , ITaxesAppService
    {
        private readonly IRepository<Taxe> _taxesRepo;

        public TaxesAppService(IRepository<Taxe> taxesRepo)
        {
            _taxesRepo = taxesRepo;
        }

       public IEnumerable<Taxe> GetTaxes()
        {
            return _taxesRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }


        public void NewTax(Taxe taxes)
        {
            AbpSession.GetUserId();
            var tax = new Taxe();

            tax.Tax = taxes.Tax;
            tax.NameOfTax = taxes.NameOfTax;
            tax.IsDeleted = false;
            tax.CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            tax.CreationTime = DateTime.Now;

            _taxesRepo.Insert(tax);
        }

        public void Update(Taxe taxes)
        {

            var tax = _taxesRepo.FirstOrDefault(i => i.Id == taxes.Id);

            tax.LastModificationTime = DateTime.Now;
            tax.LastModifierUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            tax.NameOfTax = taxes.NameOfTax;
            tax.Tax = taxes.Tax;
            _taxesRepo.Update(tax);
        }

        public void Delete(Taxe input)
        {
            var del = _taxesRepo.FirstOrDefault(i => i.Id == input.Id);
            del.IsDeleted = true;
            del.DeletionTime = DateTime.Now;
            del.DeleterUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            _taxesRepo.Update(del);
        }

    }
}
