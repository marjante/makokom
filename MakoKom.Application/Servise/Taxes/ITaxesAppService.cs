﻿using Abp.Application.Services;
using MakoKom.Entities;
using MakoKom.Servise.Taxes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise
{
 public interface ITaxesAppService : IApplicationService
    {
        IEnumerable<Taxe> GetTaxes();
        void NewTax(Taxe input);
        void Update(Taxe taxes);
        void Delete(Taxe input);

    }
}
