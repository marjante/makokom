﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MakoKom.Entities;
using MakoKom.MyEntities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Articals.Dto
{
    [AutoMapFrom(typeof(Artical))]
    public class ArticalDto : BaseAuditedEntityDto, IEntityDto
    {
        public string Name { get; set; }
        public double Stash { get; set; }
        public double PriceWithoutTax { get; set; }
        public string Measure { get; set; }
        public string Description { get; set; }
        public int  TaxeId { get; set; }
        public virtual Taxe Taxe { get; set; }
    }
}
