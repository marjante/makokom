﻿using Abp.Domain.Repositories;
using MakoKom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Articals
{
    public class ArticalsAppService : MakoKomAppServiceBase, IArticalsAppService
    {
        private readonly IRepository<Artical> _articalRepo;
        private readonly IRepository<Taxe> _taxeRepo;
        private readonly IRepository<Measures> _measureRepo;

        public ArticalsAppService(IRepository<Artical> articalRepo, IRepository<Taxe> taxeRepo, IRepository<Measures> measureRepo)
        {
            _articalRepo = articalRepo;
            _taxeRepo = taxeRepo;
            _measureRepo = measureRepo;
        }
        public IEnumerable<Artical> GetAll()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures)
                .Where(i => i.IsDeleted == false).OrderBy(i => i.Name).ToList();
            return result;
        }
        public IEnumerable<Artical> GetAll2()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures)
                .Where(i => i.IsDeleted == false).OrderByDescending(i => i.Name).ToList();
            return result;
        }

        public void AddArtical(Artical input)
        {
            
            var artical = new Artical
            {
                Name = input.Name,

                PriceWithoutTax = input.PriceWithoutTax,
                Description = input.Description,
                Stash = input.Stash,
                CreationTime = DateTime.Now,
                CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1,
                TaxeId = input.TaxeId,
                Taxe = _taxeRepo.FirstOrDefault(i => i.Id == input.TaxeId),
                IsDeleted = false,
                MeasuresId = input.MeasuresId,
                Measures = _measureRepo.FirstOrDefault(i => i.Id == input.MeasuresId),
                CalculatePrice = Math.Round(input.PriceWithoutTax + (input.PriceWithoutTax * _taxeRepo.Get(input.TaxeId).Tax / 100),2),
                LastModificationTime = DateTime.Now
                
                
                

            };
      
            _articalRepo.Insert(artical);
        }

        public void Update(Artical input)
        {
            var artical = _articalRepo.Get(input.Id);
            artical.LastModificationTime = DateTime.Now;
            artical.LastModifierUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            artical.Name = input.Name;
            artical.PriceWithoutTax = input.PriceWithoutTax;
            artical.MeasuresId = input.MeasuresId;
            artical.Measures = _measureRepo.FirstOrDefault(i => i.Id == input.MeasuresId);
            artical.Stash = input.Stash;
            artical.Description = input.Description;
            artical.TaxeId = input.TaxeId;
            artical.Taxe = _taxeRepo.FirstOrDefault(i => i.Id == input.TaxeId);
            artical.CalculatePrice = Math.Round( input.PriceWithoutTax + (input.PriceWithoutTax * _taxeRepo.Get(input.TaxeId).Tax / 100),2);
            _articalRepo.Update(artical);
        }

        public void Delete(Artical input)
        {
            var del = _articalRepo.FirstOrDefault(i => i.Id == input.Id);
            del.IsDeleted = true;
            del.DeletionTime = DateTime.Now;
            del.DeleterUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;

            _articalRepo.Update(del);

        }
        public  IEnumerable<Taxe> GetTaxe()
        {
            return _taxeRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }
        public IEnumerable<Measures> GetMesures()
        {
            return _measureRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }

        public IEnumerable<Artical> SearchArtical(string input)
        {
            var result = _articalRepo.GetAllIncluding(i => i.Taxe, i => i.Measures)
                .Where(i =>i.Name.ToUpper().Contains(input.Trim().ToUpper()))
                             .Where(i => i.IsDeleted == false)
                             .ToList();
            return result;
                             
        }
        public IEnumerable<Artical> OutOfStock()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures)
                .Where(i => i.IsDeleted == false).Where(i => i.Stash <= 10).OrderBy(i => i.Name).ToList();
            return result;
        }
       public int CountArticals()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures)
                            .Where(i => i.IsDeleted == false).OrderBy(i => i.Name).ToList();
            return result.Count();
        }
        public int CountOutOfStock()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures)
                            .Where(i => i.IsDeleted == false).Where(i => i.Stash <= 10).OrderBy(i => i.Name).ToList();
            return result.Count();
        }
        public IEnumerable<Artical> GetByDescription()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderBy(i => i.Description).ToList();
            return result;
        }
        public IEnumerable<Artical> GetByDescription2()
        {
            var result = _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderByDescending(i => i.Description).ToList();
            return result;
        }
        public IEnumerable<Artical> GetByStash()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderBy(i => i.Stash).ToList();
        }
        public IEnumerable<Artical> GetByStash2()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderByDescending(i => i.Stash).ToList();
        }
        public IEnumerable<Artical> SortByPrice()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderBy(i => i.PriceWithoutTax).ToList();
        }
        public IEnumerable<Artical> SortByPrice2()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderByDescending(i => i.PriceWithoutTax).ToList();
        }
        public IEnumerable<Artical> SortByPriceWithTax()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderBy(i => i.CalculatePrice).ToList();
        }
        public IEnumerable<Artical> SortByPriceWithTax2()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderByDescending(i => i.CalculatePrice).ToList();
        }
        public IEnumerable<Artical> SortByDate()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderBy(i => i.LastModificationTime).ToList();
        }
        public IEnumerable<Artical> SortByDate2()
        {
            return _articalRepo.GetAllIncluding(x => x.Taxe, x => x.Measures).Where(i => i.IsDeleted == false).OrderByDescending(i => i.LastModificationTime).ToList();
        }
    }
}
