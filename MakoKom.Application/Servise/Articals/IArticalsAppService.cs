﻿using Abp.Application.Services;
using MakoKom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Articals
{
    public interface IArticalsAppService : IApplicationService
    {
        IEnumerable<Artical> GetAll();
        void AddArtical(Artical input);
        IEnumerable<Taxe> GetTaxe();
        void Delete(Artical input);
        void Update(Artical input);
        IEnumerable<Measures> GetMesures();
        IEnumerable<Artical> SearchArtical(string input);
        IEnumerable<Artical> OutOfStock();
        int CountArticals();
        int CountOutOfStock();
        IEnumerable<Artical> GetByDescription();
        IEnumerable<Artical> GetByStash();
        IEnumerable<Artical> GetByStash2();
        IEnumerable<Artical> GetByDescription2();
        IEnumerable<Artical> GetAll2();
        IEnumerable<Artical> SortByPrice();
        IEnumerable<Artical> SortByPrice2();
        IEnumerable<Artical> SortByPriceWithTax();
        IEnumerable<Artical> SortByPriceWithTax2();
        IEnumerable<Artical> SortByDate();
        IEnumerable<Artical> SortByDate2();
    }
}
