﻿using Abp.Application.Services;
using MakoKom.Entities;
using MakoKom.Servise.Partners.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Partners
{
   public interface IPartnersAppService : IApplicationService
    {
        IEnumerable<Partner> GetPartners();
        void Create(Partner input);
        void Update(Partner input);
        void Delete(Partner input);
        IEnumerable<Partner> SearchPartner(string input);
        int CountPartners();


    }
}
