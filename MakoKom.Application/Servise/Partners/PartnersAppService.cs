﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using MakoKom.Entities;
using MakoKom.Servise.Partners.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Partners
{
   public class PartnersAppService : MakoKomAppServiceBase, IPartnersAppService
    {
        private readonly IRepository<Partner> _partnersRepo;

        public PartnersAppService(IRepository<Partner> partnersRepo)
        {
            _partnersRepo = partnersRepo;
        }

        public IEnumerable<Partner> GetPartners()
        {
            var result = _partnersRepo.GetAll().Where(i => i.IsDeleted == false).OrderBy(i=> i.Name).ToList();
            return result;
        }


        public void Create (Partner input)
        {

            var partner = new Partner
            {
                Name = input.Name,
                Company = input.Company,
                Adress = input.Adress,
                Email = input.Email,
                AccountNubmer = input.AccountNubmer,
                TaxNumber = input.TaxNumber,
                Phone = input.Phone,
                IsDeleted = false,
                CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1,
                Percent = input.Percent

            };
            _partnersRepo.Insert(input);
        }
        
        public void Update (Partner input)
        {
            var partners = _partnersRepo.FirstOrDefault(i => i.Id == input.Id);
            partners.Name = input.Name;
            partners.Phone = input.Phone;
            partners.Email = input.Email;
            partners.AccountNubmer = input.AccountNubmer;
            partners.Company = input.Company;
            partners.TaxNumber = input.TaxNumber;
            partners.Adress = input.Adress;
            partners.LastModificationTime = DateTime.Now;
            partners.LastModifierUserId =  AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            partners.Percent = input.Percent;
    
   
        }

        public void Delete(Partner input)
        {
            var del = _partnersRepo.FirstOrDefault(i => i.Id == input.Id);

            del.DeletionTime = DateTime.Now;
            del.IsDeleted = true;
            del.DeleterUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1; 

        }
        public IEnumerable<Partner> SearchPartner(string input)
        {
            var result = _partnersRepo.GetAll().Where(s =>
           s.Name.ToUpper().Contains(input.Trim().ToUpper())||
           s.Company.ToUpper().Contains(input.Trim().ToUpper()))
           .Where(i => i.IsDeleted ==  false).ToList();
            return result;
        }
        public int CountPartners()
        {
          var count = _partnersRepo.GetAll().Where(i => i.IsDeleted == false).OrderBy(i => i.Name).ToList();
            return count.Count();
        }
    }
}
