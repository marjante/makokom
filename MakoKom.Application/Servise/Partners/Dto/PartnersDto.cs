﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MakoKom.Entities;
using MakoKom.MyEntities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Partners.Dto
{
    [AutoMapFrom(typeof(Partner))]
    public class PartnersDto : BaseAuditedEntityDto , IEntityDto
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Adress { get; set; }
        public int Phone { get; set; }
        public int AccountNubmer { get; set; }
        public string TaxNumber { get; set; }
        public string Email { get; set; }
        public int Percent { get; set; }
    }
}
