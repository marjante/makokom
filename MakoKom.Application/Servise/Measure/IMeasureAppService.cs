﻿using Abp.Application.Services;
using MakoKom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Measure
{
   public  interface IMeasureAppService : IApplicationService
    {
        IEnumerable<Measures> GetAll();
        void Delete(Measures input);
        void Update(Measures input);
        void Create(Measures input);
    }
}
