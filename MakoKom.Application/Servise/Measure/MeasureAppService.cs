﻿using Abp.Domain.Repositories;
using MakoKom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Measure
{
   public class MeasureAppService : MakoKomAppServiceBase, IMeasureAppService 
    {
        private readonly IRepository<Measures> _measureRepo;

        public MeasureAppService(IRepository<Measures> measureRepo)
        {
            _measureRepo = measureRepo;
        } 

        public IEnumerable<Measures> GetAll()
        {
            return _measureRepo.GetAll().Where(i => i.IsDeleted == false).ToList();
        }

        public void Create(Measures input)
        {
            var measure = new Measures
            {
                MeasureName = input.MeasureName,
                CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1,
                IsDeleted = false,

            };
            _measureRepo.Insert(measure);
        }
        public void Update(Measures input)
        {
            var measure = _measureRepo.FirstOrDefault(i => i.Id == input.Id);
            measure.MeasureName = input.MeasureName;
            measure.LastModifierUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            measure.LastModificationTime = DateTime.Now;
            _measureRepo.Update(measure);

        }

        public void Delete (Measures input)
        {
            var del = _measureRepo.FirstOrDefault(i => i.Id == input.Id);
            del.IsDeleted = true;
            del.DeleterUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
            del.DeletionTime = DateTime.Now;
            _measureRepo.Update(del);
        }
    }
}
