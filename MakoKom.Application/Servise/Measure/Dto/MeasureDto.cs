﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MakoKom.Entities;
using MakoKom.MyEntities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakoKom.Servise.Measure.Dto
{
    [AutoMapFrom(typeof(Measures))]
    public class MeasureDto : BaseAuditedEntityDto , IEntityDto
    {
        public string MeasureName { get; set; }
    }
}
