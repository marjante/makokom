﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using MakoKom.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MakoKom.MyEntities.BaseEntity
{
    [AutoMapFrom(typeof(BaseAuditedEntity))]
    public abstract class BaseAuditedEntityDto : MakoKomAppServiceBase, IEntity, ICreationTime, ICreatedBy
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long LastModifierUserId { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public virtual long CreatorUserId { get; set; }


        public BaseAuditedEntityDto()
        {
            CreationTime = DateTime.Now;
            CreatorUserId = AbpSession.UserId != null ? (long)AbpSession.UserId : 1;
        }


        public bool IsTransient()
        {
            return true;
        }
    }
}
